# Gitlab-runner for privileged jobs

This project keeps a gitlab-runner image with the required scripts for registration and running jobs.

As of March 2019, we have to build our own `docker-machine` binary to include latest changes that were not yet added to a release.

This image also has an extension of the upstream gitlab-runner image with nss_wrapper, to allow using ssh on the Openshift pods (Docker-machine requirement).

# Versioning

**Each branch should be named according to the upstream GitLab-runner version, so Docker tags it accordingly when pushing to GitLab registry**.

This tags will be used for updating the privileged runner deployment on <https://gitlab.cern.ch/vcs/oo-gitlab-cern>.